// Copyright 2016 Robert Maier, Technical University Munich
#include "tum_benchmark.h"

#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <fstream>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


TumBenchmark::TumBenchmark()
{
}


TumBenchmark::~TumBenchmark()
{
}


bool TumBenchmark::loadAssoc(const std::string &assocFile,
	std::vector<double> &timestampsDepth, std::vector<std::string> &filesDepth,
	std::vector<double> &timestampsColor, std::vector<std::string> &filesColor) const
{
	if (assocFile.empty())
		return false;

	//load transformations from CVPR RGBD datasets benchmark
	std::ifstream assocIn;
	assocIn.open(assocFile.c_str());
	if (!assocIn.is_open())
		return false;

	// first load all groundtruth timestamps and poses
	std::string line;
	while (std::getline(assocIn, line))
	{
		if (line.empty() || line.compare(0, 1, "#") == 0)
			continue;
		std::istringstream iss(line);
		double timestampDepth, timestampColor;
		std::string fileDepth, fileColor;
		if (!(iss >> timestampColor >> fileColor >> timestampDepth >> fileDepth))
			break;

		timestampsDepth.push_back(timestampDepth);
		filesDepth.push_back(fileDepth);
		timestampsColor.push_back(timestampColor);
		filesColor.push_back(fileColor);
	}
	assocIn.close();

	return true;
}


cv::Mat TumBenchmark::loadColor(const std::string &filename)
{
    cv::Mat imgColor = cv::imread(filename);
    // convert gray to float
    cv::Mat color;
    imgColor.convertTo(color, CV_32FC3, 1.0f / 255.0f);
    return color;
}

cv::Mat TumBenchmark::loadColor_CV_32FC1(const std::string &filename)
{
    cv::Mat imgColor = cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
    // convert gray to float
    cv::Mat color;
    imgColor.convertTo(color, CV_32FC1, 1.0f / 255.0f);
    return color;
}



cv::Mat TumBenchmark::loadDepth(const std::string &filename)
{
    //fill/read 16 bit depth image
    cv::Mat imgDepthIn = cv::imread(filename, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
    cv::Mat imgDepth;
    imgDepthIn.convertTo(imgDepth, CV_32FC1, (1.0 / 5000.0));
    //imgDepthIn.convertTo(imgDepth, CV_32FC1, (1.0 / 5000.0));
    return imgDepth;
}


bool TumBenchmark::loadCamIntrinsics(const std::string &filename, Eigen::Matrix3d &K)
{
    std::ifstream camFile(filename.c_str());
    if (!camFile.is_open())
        return false;

    //camera width and height
    int w, h;
    camFile >> w >> h;
    //camera intrinsics
    float fVal = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            camFile >> fVal;
            K(i, j) = fVal;
        }
    }
#if 0
    //distortion coefficients
    for (int i = 0; i < 5; i++) {
        camFile >> fVal;
        m_distCoeffs.at<double>(0, i) = fVal;
    }
#endif
    camFile.close();

    return true;
}


bool TumBenchmark::savePoses(const std::string &filename, const std::vector<Eigen::Matrix4f> &poses, const std::vector<double> &timestamps)
{
    if (filename.empty())
        return false;
    if (timestamps.empty() || timestamps.size() != poses.size())
        return false;

    // open output file for TUM RGB-D benchmark poses
    std::ofstream outFile;
    outFile.open(filename.c_str());
    if (!outFile.is_open())
        return false;

    // write poses into output file
    outFile << std::fixed << std::setprecision(6);
    for (size_t i = 0; i < poses.size(); i++)
    {
        Eigen::Matrix4f pose = poses[i];

        //write into evaluation file
        //timestamp
        double timestamp = timestamps[i];
        outFile << timestamp << " ";

        //translation
        Eigen::Vector3f translation = pose.topRightCorner(3,1);
        outFile << translation[0] << " " << translation[1] << " " << translation[2];
        //rotation (quaternion)
        Eigen::Matrix3f rot = pose.topLeftCorner(3,3);
        Eigen::Quaternionf quat(rot);
        outFile << " " << quat.x() << " " << quat.y() << " " << quat.z() << " " << quat.w() << std::endl;
    }
    outFile.close();

    return true;
}
