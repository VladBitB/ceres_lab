// Copyright 2016 Robert Maier, Technical University Munich
#ifndef RGBD_PROCESSING_H
#define RGBD_PROCESSING_H

#ifndef WIN64
    #define EIGEN_DONT_ALIGN_STATICALLY
#endif
#include <Eigen/Dense>

#include <opencv2/core/core.hpp>


class RgbdProcessing
{
public:
    RgbdProcessing();
    ~RgbdProcessing();
    
    //bool RgbdProcessing::computeVertexMapMask(const Eigen::Matrix3f &K, const cv::Mat &depth, const cv::Mat &mask, cv::Mat &vertexMap);
    
    bool computeVertexMap(const Eigen::Matrix3d &K, const cv::Mat &depth, cv::Mat &vertexMap);
    
    void computeNormals(const cv::Mat &vertexMap, cv::Mat &normals, float depthThreshold = 0.3f) const;
    void computeNormalsCov(const cv::Mat &vertexMap, cv::Mat &normals, int windowSize = 3, float depthThreshold = 0.3f) const;
    
    void visualizeNormals(const cv::Mat &normals, cv::Mat &normalsVis) const;
    
    void threshold(cv::Mat &depth, float depthMin, float depthMax) const;
    
    cv::Mat downsampleDepth(const cv::Mat &depth) const;

    Eigen::Matrix3f downsampleK(const Eigen::Matrix3f &K) const;

private:

};

#endif
