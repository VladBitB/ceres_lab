// Copyright 2016 Robert Maier, Technical University Munich
#include "rgbd_processing.h"

#include <iostream>
#include <sstream>
#include <string>

#include <opencv2/imgproc/imgproc.hpp>


RgbdProcessing::RgbdProcessing()
{
}


RgbdProcessing::~RgbdProcessing()
{
}




/*
bool RgbdProcessing::computeVertexMapMask(const Eigen::Matrix3f &K, const cv::Mat &depth, const cv::Mat &mask, cv::Mat &vertexMap)
{
    int w = depth.cols;
    int h = depth.rows;
    float cx = K(0, 2);
    float cy = K(1, 2);
    float fxInv = 1.0f / K(0, 0);
    float fyInv = 1.0f / K(1, 1);
    
    cv::Mat vertexMap_ = cv::Mat::zeros(h, w, CV_32FC3);
    float* ptrVert = (float*)vertexMap.data;
    const float* ptrDepth = (const float*)depth.data;
    
    
    long counter = 0; 
    
    for (int y = 0; y < h; ++y)
    {
        for (int x = 0; x < w; ++x)
        {
            if (mask.at<uchar>(y, x) == 255){
            
                float depthVal = ptrDepth[y*w + x];
                float x0 = (float(x) - cx) * fxInv;
                float y0 = (float(y) - cy) * fyInv;
                //float scale = std::sqrt(x0*x0 + y0*y0 + 1.0f);
                //scale = 1.0;
                //depthVal = depthVal * scale;
    
                //size_t off = (y*w + x) * 3;
                size_t off = counter * 3;
                ptrVert[off] = x0 * depthVal;
                ptrVert[off+1] = y0 * depthVal;
                ptrVert[off+2] = depthVal;
            
                counter++;
                
            }
            
        }
    }
    
    
    vertexMap = cv::Mat::zeros(h, w, CV_32FC3); 
    
    
    
    
    return true;
}
*/



bool RgbdProcessing::computeVertexMap(const Eigen::Matrix3d &K, const cv::Mat &depth, cv::Mat &vertexMap)
{
    int w = depth.cols;
    int h = depth.rows;
    float cx = K(0, 2);
    float cy = K(1, 2);
    float fxInv = 1.0f / K(0, 0);
    float fyInv = 1.0f / K(1, 1);
    
    vertexMap = cv::Mat::zeros(h, w, CV_32FC3);
    float* ptrVert = (float*)vertexMap.data;
    const float* ptrDepth = (const float*)depth.data;
    for (int y = 0; y < h; ++y)
    {
        for (int x = 0; x < w; ++x)
        {
            float depthVal = ptrDepth[y*w + x];
            float x0 = (float(x) - cx) * fxInv;
            float y0 = (float(y) - cy) * fyInv;
            //float scale = std::sqrt(x0*x0 + y0*y0 + 1.0f);
            //scale = 1.0;
            //depthVal = depthVal * scale;
    
            size_t off = (y*w + x) * 3;
            ptrVert[off] = x0 * depthVal;
            ptrVert[off+1] = y0 * depthVal;
            ptrVert[off+2] = depthVal;
        }
    }
    
    return true;
}


void RgbdProcessing::computeNormals(const cv::Mat &vertexMap, cv::Mat &normals, float depthThreshold) const
{
    int w = vertexMap.cols;
    int h = vertexMap.rows;
    normals = cv::Mat::zeros(h, w, CV_32FC3);
    
    const float* ptrVert = (const float*)vertexMap.data;
    float* ptrNormals = (float*)normals.data;
    
    // Depth threshold to avoid computing properties over depth-discontinuities
    for (int y = 1; y < h-1; ++y)
    {
        for (int x = 1; x < w-1; ++x)
        {
            size_t off = (y*w + x) * 3;
            Eigen::Vector3f vert(ptrVert[off], ptrVert[off+1], ptrVert[off+2]);
            if (std::isnan(vert[2]) || vert[2] == 0.0f)
                continue;
            
            // determine tangent vectors
            size_t offX0 = (y*w + x-1) * 3;
            Eigen::Vector3f vertX0(ptrVert[offX0], ptrVert[offX0+1], ptrVert[offX0+2]);
            size_t offX1 = (y*w + x+1) * 3;
            Eigen::Vector3f vertX1(ptrVert[offX1], ptrVert[offX1+1], ptrVert[offX1+2]);
            size_t offY0 = ((y-1)*w + x) * 3;
            Eigen::Vector3f vertY0(ptrVert[offY0], ptrVert[offY0+1], ptrVert[offY0+2]);
            size_t offY1 = ((y+1)*w + x) * 3;
            Eigen::Vector3f vertY1(ptrVert[offY1], ptrVert[offY1+1], ptrVert[offY1+2]);
            if (std::isnan(vertX0[2]) || std::isnan(vertX1[2]) || std::isnan(vertY0[2]) || std::isnan(vertY1[2]))
                continue;
            if (vertX0[2] == 0.0f || vertX1[2] == 0.0f || vertY0[2] == 0.0f || vertY1[2] == 0.0f)
                continue;
            
            Eigen::Vector3f tangentX = vertX1-vertX0;
            Eigen::Vector3f tangentY = vertY1-vertY0;
            if (tangentX.norm() < depthThreshold && tangentY.norm() < depthThreshold)
            {
                // compute normal using cross product
                Eigen::Vector3f normal = (tangentY.cross(tangentX)).normalized();
                ptrNormals[off] = normal[0];
                ptrNormals[off+1] = normal[1];
                ptrNormals[off+2] = normal[2];
            }
        }
    }
}



void RgbdProcessing::computeNormalsCov(const cv::Mat &vertexMap, cv::Mat &normals, int windowSize, float depthThreshold) const
{
    int w = vertexMap.cols;
    int h = vertexMap.rows;
    normals = cv::Mat::zeros(h, w, CV_32FC3);
    
    int windowHalf = windowSize / 2;
    
    const float* ptrVert = (const float*)vertexMap.data;
    float* ptrNormals = (float*)normals.data;
    
    Eigen::Vector3f viewpoint(0.0f, 0.0f, 0.0f);
    
    // Depth threshold to avoid computing properties over depth-discontinuities
    for (int y = windowHalf; y < h-windowHalf; ++y)
    {
        for (int x = windowHalf; x < w-windowHalf; ++x)
        {
            size_t off = (y*w + x) * 3;
            Eigen::Vector3f vert(ptrVert[off], ptrVert[off+1], ptrVert[off+2]);
            if (std::isnan(vert[2]) || vert[2] == 0.0f)
                continue;
            
            // compute centroid
            Eigen::Vector3f c(0.0f, 0.0f, 0.0f);
            int cnt = 0;
            for (int v = -windowHalf; v <= windowHalf; ++v)
            {
                for (int u = -windowHalf; u <= windowHalf; ++u)
                {
                    int lx = x + u;
                    int ly = y + v;
                    //if (lx == x && ly == y)
                    //    continue;
                    
                    size_t offL = (ly*w + lx) * 3;
                    Eigen::Vector3f pt(ptrVert[offL], ptrVert[offL+1], ptrVert[offL+2]);
                    if (pt[2] != 0.0 && !std::isnan(pt[2]) && (vert - pt).norm() <= depthThreshold)
                    {
                        c += pt;
                        ++cnt;
                    }
                }
            }
            c /= static_cast<float>(cnt);
            //std::cout << "centroid: " << c.transpose() << std::endl;
            
            // compute covariance matrix
            Eigen::Matrix3f covMat;
            covMat.setZero();
            cnt = 0;
            for (int v = -windowHalf; v <= windowHalf; ++v)
            {
                for (int u = -windowHalf; u <= windowHalf; ++u)
                {
                    int lx = x + u;
                    int ly = y + v;
                    //if (lx == x && ly == y)
                    //    continue;
                    
                    size_t offL = (ly*w + lx) * 3;
                    Eigen::Vector3f pt(ptrVert[offL], ptrVert[offL+1], ptrVert[offL+2]);
                    if (pt[2] != 0.0 && !std::isnan(pt[2]) && (vert - pt).norm() <= depthThreshold)
                    {
                        Eigen::Vector3f ptCov = pt - c;
                        
                        covMat(0, 0) += ptCov.x() * ptCov.x();
                        covMat(0, 1) += ptCov.x() * ptCov.y();
                        covMat(0, 2) += ptCov.x() * ptCov.z();
                        covMat(1, 1) += ptCov.y() * ptCov.y();
                        covMat(1, 2) += ptCov.y() * ptCov.z();
                        covMat(2, 2) += ptCov.z() * ptCov.z();
                        ++cnt;
                    }
                }
            }
            // mirror covariance matrix
            covMat(1, 0) = covMat(0, 1);
            covMat(2, 0) = covMat(0, 2);
            covMat(2, 1) = covMat(1, 2);
            // normalize covariance matrix
            covMat /= static_cast<float>(cnt);
            //std::cout << "covariance: " << std::endl << covMat << std::endl;

            // compute normal using eigen value decomposition
            Eigen::Vector3f normal(0.0f, 0.0f, 0.0f);
            Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigenSolver(covMat);
            if (eigenSolver.info() == Eigen::Success)
            {
                // eigen values
                //Eigen::Vector3f eigenValues = eigenSolver.eigenvalues();
                //std::cout << "eigenvalues" << eigenSolver.eigenvalues() << std::endl;
                
                // eigen vectors
                //std::cout << "eigenvectors" << eigenSolver.eigenvectors() << std::endl;
                //Eigen::Matrix3f eigenVectors = eigenSolver.eigenvectors();
                Eigen::Vector3f eigenVec0 = eigenSolver.eigenvectors().col(0);
                //std::cout << "eigenvector 0" << eigenVec0 << std::endl;
                normal = eigenVec0.normalized();
                
                // orient normal
                if (normal.dot(viewpoint - vert) <= 0.0f)
                    normal = -normal;
            }
            //std::cout << "normal: " << normal.transpose() << std::endl;
            
            // store normal
            ptrNormals[off] = normal[0];
            ptrNormals[off+1] = normal[1];
            ptrNormals[off+2] = normal[2];
        }
    }
}


void RgbdProcessing::visualizeNormals(const cv::Mat &normals, cv::Mat &normalsVis) const
{
#if 1
    cv::cvtColor(normals, normalsVis, CV_BGR2RGB);
    normalsVis = (-0.5f * normalsVis) + 0.5f;
#else
    int w = vertexMap.cols;
    int h = vertexMap.rows;
    normalsVis = cv::Mat::zeros(h, w, CV_32FC3);
    
    const float* ptrNormals = (const float*)normals.data;
    float* ptrNormalsVis = (float*)normalsVis.data;
    
    // Depth threshold to avoid computing properties over depth-discontinuities
    for (int y = 0; y < h; ++y)
    {
        for (int x = 0; x < w; ++x)
        {
            Eigen::Vector3f normalVis(0.0f, 0.0f, 0.0f);
            size_t off = (y*w + x) * 3;
            Eigen::Vector3f normal(ptrNormals[off], ptrNormals[off+1], ptrNormals[off+2]);
            if (!std::isnan(normal[2]) && normal[2] != 0.0f)
            {
                normalVis[0] = -0.5f * normal[0] + 0.5f;
                normalVis[1] = -0.5f * normal[1] + 0.5f;
                normalVis[2] = -0.5f * normal[2] + 0.5f;
            }
            ptrNormalsVis[off] = normalVis[0];
            ptrNormalsVis[off+1] = normalVis[1];
            ptrNormalsVis[off+2] = normalVis[2];
        }
    }
#endif
}


void RgbdProcessing::threshold(cv::Mat &depth, float depthMin, float depthMax) const
{
    cv::threshold(depth, depth, depthMin, 0.0, cv::THRESH_TOZERO);
    cv::threshold(depth, depth, depthMax, 0.0, cv::THRESH_TOZERO_INV);
}


cv::Mat RgbdProcessing::downsampleDepth(const cv::Mat &depth) const
{
    const float* ptrIn = (const float*)depth.data;
    int w = depth.cols;
    int h = depth.rows;
    int wDown = w/2;
    int hDown = h/2;

    // downscaling by averaging the inverse depth
    cv::Mat depthDown = cv::Mat::zeros(hDown, wDown, depth.type());
    float* ptrOut = (float*)depthDown.data;
    for (size_t y = 0; y < hDown; ++y)
    {
        for (size_t x = 0; x < wDown; ++x)
        {
            float d0 = ptrIn[2*y * w + 2*x];
            float d1 = ptrIn[2*y * w + 2*x+1];
            float d2 = ptrIn[(2*y+1) * w + 2*x];
            float d3 = ptrIn[(2*y+1) * w + 2*x+1];

            int cnt = 0;
            float sum = 0.0f;
            if (d0 != 0.0f)
            {
                sum += 1.0f / d0;
                ++cnt;
            }
            if (d1 != 0.0f)
            {
                sum += 1.0f / d1;
                ++cnt;
            }
            if (d2 != 0.0f)
            {
                sum += 1.0f / d2;
                ++cnt;
            }
            if (d3 != 0.0f)
            {
                sum += 1.0f / d3;
                ++cnt;
            }

            if (cnt > 0)
            {
                float dInv = sum / float(cnt);
                if (dInv != 0.0f)
                    ptrOut[y*wDown + x] = 1.0f / dInv;
            }
        }
    }

    return depthDown;
}


Eigen::Matrix3f RgbdProcessing::downsampleK(const Eigen::Matrix3f &K) const
{
    Eigen::Matrix3f kDown = K;
    kDown(0, 2) += 0.5f;
    kDown(1, 2) += 0.5f;
    kDown.topLeftCorner(2, 3) = kDown.topLeftCorner(2, 3) * 0.5f;
    kDown(0, 2) -= 0.5f;
    kDown(1, 2) -= 0.5f;
    return kDown;
}
