/*
 * dvo_ceres_settings.cpp
 * settings module for the direct dense rgb-d image alignment
 * 
 * created: 13.01.2017
 * 
 */

#include "dvo_ceres_settings.hpp"


// ### load settings
void conf_dvo_ceres::load_dvo_ceres_settings(string config_file){ 
        
        libconfig::Config cfg; 
        
        try{ cfg.readFile(config_file.c_str());}
        catch(const libconfig::FileIOException &fioex){
            cerr << "\033[0;31m-> config file not found\033[0m" << endl;
            exit(EXIT_FAILURE);
        }
        catch (const libconfig::ParseException &pe){
            cerr << "\033[0;31m-> failed to read settings file\033[0m" << endl;
            cerr << "\033[0;31m  -> possible reason: unable to parse the file correctly\033[0m" << endl;
            exit(EXIT_FAILURE);
        }
        
        try{
            
            cfg.readFile(config_file.c_str());
            libconfig::Setting &root = cfg.getRoot(); 
            
            // strings
            const char *param1  = root["data_folder"]; 
            //const char *param2  = root["rgb_file"]; 
            //const char *param3  = root["depth_file"]; 
            const char *param4  = root["data_assoc_file"]; 
            const char *param5  = root["intrinsics_file"]; 
            
            _data_folder        = param1;    
            //_rgb_file           = param2;    
            //_depth_file         = param3;    
            _data_assoc_file    = param4;  
            _intrinsics_file    = param5; 
            
            // doubles
            _invalid_residual   = root["invalid_residual"]; 
            _alpha              = root["alpha"]; 
            _beta               = root["beta"]; 
           _convergenceErrorRatio = root["convergenceErrorRatio"]; 
           
           /* 
            libconfig::Setting &param8 = root["intrinsics"]; 
            
            for (uint i = 0; i < 9; i++)
                _intrinsics[i] = param8[i]; 
           */
           
            // uints
            _depth_filter_type  = root["depth_filter_type"]; 
            _num_levels         = root["num_levels"]; 
            _max_iterations     = root["max_iterations"];
            _use_Huber          = root["use_Huber"];
            _early_break        = root["early_break"]; 
            
            // displaying flag
            dump_dvo_ceres_settings_ = root["dump_dvo_ceres_settings"];
            
        }
        
	catch(const libconfig::FileIOException &fioex){
		cerr << "-> config file not found" << endl;
		exit(EXIT_FAILURE);
	}
	catch(const libconfig::ParseException &spe){
	  cerr << "-> failed to read settings file" << endl;
	  cerr << "  -> parse exeption" << endl;
	  exit(EXIT_FAILURE);
	}
	catch( const libconfig::SettingTypeException &ste ){
	  cerr << "-> failed to read settings file" << endl;
	  cerr << "  -> possible reason: type mismatch (e.g. float parameters should contain a \".\")" << endl;
	  exit(EXIT_FAILURE);  
	}
	catch (const libconfig::SettingNotFoundException &snf){
	  cerr << "-> failed to read settings file" << endl;
	  cerr << "  -> a setting accidently deleted from the settings file" << endl;
	  exit(EXIT_FAILURE);
	}
	catch(...){
	  cerr << "-> failed to read settings file" << endl;
	  cerr << "  -> unknown reason, the file is corrupted" << endl;
	  exit(EXIT_FAILURE);
	}

}




// ### display settings
void conf_dvo_ceres::dump_dvo_ceres_settings(){ 
        
        cout << endl;
        cout << std::left << std::setfill(' ');
        cout << " -> Program settings:" 								       << endl; 
        cout << setw(cout_align) << "  -> data_folder: " 		        << setw(12) << _data_folder    << endl; 
        //cout << setw(cout_align) << "  -> rgb_file: " 		                << setw(12) << _rgb_file << endl; 
        //cout << setw(cout_align) << "  -> depth_file: " 		        << setw(12) << _depth_file       << endl; 
        cout << setw(cout_align) << "  -> data assoc_file: " 		        << setw(12) << _data_assoc_file  << endl; 
        cout << setw(cout_align) << "  -> intrinsics_file: " 		        << setw(12) << _intrinsics_file  << endl; 
        cout << endl;
        
        cout << setw(cout_align) << "  -> invalid_residual: " 	                << setw(12) << _invalid_residual << endl; 
        cout << setw(cout_align) << "  -> alpha: " 	                        << setw(12) << _alpha            << endl; 
        cout << setw(cout_align) << "  -> beta: " 	                        << setw(12) << _beta             << endl; 
        cout << setw(cout_align) << "  -> convergenceErrorRatio: " 	        << setw(12) << _convergenceErrorRatio << endl; 
        cout << endl;
        /*
        cout << setw(cout_align) << "  -> intrinsics: " 	                << endl;
        
        cout << _intrinsics[0] << " " << _intrinsics[1] << " " << _intrinsics[2] << endl <<  
                _intrinsics[3] << " " << _intrinsics[4] << " " << _intrinsics[5] << endl << 
                _intrinsics[6] << " " << _intrinsics[7] << " " << _intrinsics[8] << endl; 
        */
        
        cout << setw(cout_align) << "  -> depth_filter_type: " 	                << setw(12) << _depth_filter_type      << endl; 
        cout << setw(cout_align) << "  -> num_levels: " 	                << setw(12) << _num_levels             << endl; 
        cout << setw(cout_align) << "  -> max_iterations: " 	                << setw(12) << _max_iterations         << endl; 
        cout << setw(cout_align) << "  -> use_Huber: " 	                        << setw(12) << _use_Huber              << endl; 
        cout << setw(cout_align) << "  -> early_break: " 	                << setw(12) << _early_break              << endl; 
        
        cout << endl;
        cout << endl;
        
}




