/*
 * dvo_ceres_settings.hpp
 * settings module for the direct dense rgb-d image alignment
 * 
 * created: 13.01.2017
 * 
 */

#ifndef DVO_CERES_SETTINGS_HPP_
#define DVO_CERES_SETTINGS_HPP_

#include <iostream>
#include <iomanip>
#include "../includes/libconfig.h++"
#include <cstdlib>

using namespace std;

#define cout_align 35

struct conf_dvo_ceres{
    
  string _data_folder;          //
  //string _rgb_file;             // later it will be probably the pase path
  //string _depth_file;           // -"-
  string _data_assoc_file;  
  string _intrinsics_file; 
  
  double _invalid_residual; 
  double _alpha, _beta; 
  double _convergenceErrorRatio; 
  //double _intrinsics[9]; 
  
  uint _depth_filter_type; 
  uint _num_levels; 
  uint _max_iterations; 
  uint _use_Huber; 
  uint _early_break; 
  
  uint dump_dvo_ceres_settings_;
  
  void load_dvo_ceres_settings(string config_file);
  void dump_dvo_ceres_settings();
}; 


#endif /* DVO_CERES_SETTINGS_HPP_ */