/*
 * ply_module.hpp
 *
 *  Created on: 	04.06.2014
 *  Last modified on: 	27.01.2015
 *      Author: golyanik
 */

#ifndef PLY_MODULE_HPP_
#define PLY_MODULE_HPP_

#include <iostream>
#include <stdio.h>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <ctime>

#include "rply.h"

using namespace std;
//using namespace Eigen;


struct ply_color{
  unsigned char red;
  unsigned char green;
  unsigned char blue;
};

// 
int read_ply_file(/*input*/
		  string f_name, 									//name of a .ply file
		  int free_vector,									//free internal memory after the last call
		  /*output*/
		  Eigen::MatrixXd &Mat, 									//point cloud, num of vertices x 3
		  Eigen::MatrixXi *Colors
		 );


int write_ply_file(/*input*/
		   string f_name,									//name of an output .ply file
		   Eigen::MatrixXd &Mat,									//point cloud data to save in a .ply file
		   Eigen::MatrixXi *Colors,									//point colors of the point cloud
		   ply_color color,
		   int no_color										//0 - no_color
													//1 - transfer colors (Colors is used)
													//2 - use the same color for all points
		  );

#endif /* PLY_MODULE_HPP_ */
