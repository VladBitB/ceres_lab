// Copyright 2016 Robert Maier, Technical University of Munich
// Copyright 2017 Vlad Golyanik, University of Kaiserslautern

#include "./main.hpp"




//
// ### bilinear interpolation; drops zero values
//
double interpolate(const float* ptrImgIntensity, double x, double y, int w, int h)
{
    double valCur = std::numeric_limits<double>::quiet_NaN();
    
#if 0
    // direct lookup, no interpolation
    int x0 = static_cast<int>(std::floor(x + 0.5));
    int y0 = static_cast<int>(std::floor(y + 0.5));
    if (x0 >= 0 && x0 < w && y0 >= 0 && y0 < h)
        valCur = ptrImgIntensity[y0*w + x0];
#else
    //bilinear interpolation
    int x0 = static_cast<int>(std::floor(x));
    int y0 = static_cast<int>(std::floor(y));
    int x1 = x0 + 1;
    int y1 = y0 + 1;
    
    double x1_weight = x - static_cast<double>(x0);
    double y1_weight = y - static_cast<double>(y0);
    double x0_weight = 1.0 - x1_weight;
    double y0_weight = 1.0 - y1_weight;
    
    if (x0 < 0 || x0 >= w)
        x0_weight = 0.0;
    if (x1 < 0 || x1 >= w)
        x1_weight = 0.0;
    if (y0 < 0 || y0 >= h)
        y0_weight = 0.0;
    if (y1 < 0 || y1 >= h)
        y1_weight = 0.0;
    double w00 = x0_weight * y0_weight;
    double w10 = x1_weight * y0_weight;
    double w01 = x0_weight * y1_weight;
    double w11 = x1_weight * y1_weight;
    
    double sumWeights = w00 + w10 + w01 + w11;
    double sum = 0.0;
    if (w00 > 0.0)
        sum += static_cast<double>(ptrImgIntensity[y0*w + x0]) * w00;
    if (w01 > 0.0)
        sum += static_cast<double>(ptrImgIntensity[y1*w + x0]) * w01;
    if (w10 > 0.0)
        sum += static_cast<double>(ptrImgIntensity[y0*w + x1]) * w10;
    if (w11 > 0.0)
        sum += static_cast<double>(ptrImgIntensity[y1*w + x1]) * w11;
    
    if (sumWeights > 0.0)
        valCur = sum / sumWeights;
#endif
    
    return valCur;
}


class DepthErrorNumeric
{
public:
    DepthErrorNumeric(cv::Mat dRef, cv::Mat dCur, int x, int y) :
        m_dRef(dRef),
        m_dCur(dCur),
        m_x(x),
        m_y(y)
    {
    }
    
    virtual ~DepthErrorNumeric() {}

    bool operator()(const double* const pt, double* residuals) const
    {
        const float* ptrImgRef = (const float*)m_dRef.data;
        const float* ptrImgCur = (const float*)m_dCur.data;
        int w = m_dRef.cols;
        int h = m_dRef.rows;

        double px = pt[0];
        double py = pt[1];
        if (px >= 0 && py >= 0 && px < w && py < h)
        {
            // lookup interpolated intensity
            double valCur = interpolate(ptrImgCur, px, py, w, h);
            if (!std::isnan(valCur))
            {
                // compute residual
                double valRef = static_cast<double>(ptrImgRef[m_y*w + m_x]);
                residuals[0] = valRef - valCur;
                return true;
            }
        }
        
        residuals[0] = 0.0;
        return true;
    }
    
protected:
    cv::Mat m_dRef;
    cv::Mat m_dCur;
    int m_x;
    int m_y;
};


class DvoErrorAutoDepth
{
public:
    
    typedef ceres::NumericDiffCostFunction<DepthErrorNumeric, ceres::CENTRAL, 1, 2>  IntensityErrorFunction;
    typedef DepthErrorNumeric DepthError;
    
    DvoErrorAutoDepth(cv::Mat grayRef, cv::Mat depthRef,
                    cv::Mat grayCur, cv::Mat depthCur,
                    const Eigen::Matrix3d &K, int x, int y) :
        m_depthRef(depthRef),
        m_depthCurrent(depthCur),
        m_grayRef(grayRef),
        m_grayCur(grayCur),
        m_K(K),
        m_x(x),
        m_y(y),
        //m_intensityError(new IntensityErrorFunction(new IntensityError(grayRef, grayCur, x, y)))
        m_intensityError(new IntensityErrorFunction(new DepthError(depthRef, depthCur, x, y)))
    {
    }
    
    virtual ~DvoErrorAutoDepth()
    {
    }
    
    template <typename T>
    bool operator()(const T* const xi, T* residuals) const
    {
        T rot[3];
        rot[0] = xi[3];
        rot[1] = xi[4];
        rot[2] = xi[5];
        T t[3];
        t[0] = xi[0];
        t[1] = xi[1];
        t[2] = xi[2];
        
        const float* ptrDepthRef = (const float*)m_depthRef.data;
        int w = m_depthRef.cols;
        int h = m_depthRef.rows;
        T cx = T(m_K(0, 2));
        T cy = T(m_K(1, 2));
        T fx = T(m_K(0, 0));
        T fy = T(m_K(1, 1));
        T fxInv = T(1.0) / fx;
        T fyInv = T(1.0) / fy;

        // get depth
        size_t off = m_y*w + m_x;
        T dRef = T(ptrDepthRef[off]);
        if (dRef > T(0.0))
        {
            // project 2d point back into 3d using its depth
            T x0 = T((T(m_x) - cx) * fxInv);
            T y0 = T((T(m_y) - cy) * fyInv);
            
            // transform reference 3d point into current frame
            // rotation
            T pt3Ref[3];
            pt3Ref[0] = x0 * dRef;
            pt3Ref[1] = y0 * dRef;
            pt3Ref[2] = dRef;
            T pt3Cur[3];
            ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
            // translation
            pt3Cur[0] += t[0];
            pt3Cur[1] += t[1];
            pt3Cur[2] += t[2];

            if (pt3Cur[2] > T(0.0))
            {
                // project 3d point to 2d
                T pt2Cur[2];
                pt2Cur[0] = T((fx * pt3Cur[0] / pt3Cur[2]) + cx);
                pt2Cur[1] = T((fy * pt3Cur[1] / pt3Cur[2]) + cy);
#if 1
                // numeric differentiation
                return m_intensityError(pt2Cur, residuals);
#else
                // automatic differentiation
                T px = pt2Cur[0];
                T py = pt2Cur[1];
                if (px >= T(0) && py >= T(0) && px <= T(w-1) && py <= T(h-1))
                {
                    const float* ptrGrayRef = (const float*)m_grayRef.data;
                    const float* ptrGrayCur = (const float*)m_grayCur.data;
                    // lookup interpolated intensity
                    typedef ceres::Grid2D<float, 1> ImageDataType;
                    ImageDataType array(ptrGrayCur, 0, h, 0, w);
                    ceres::BiCubicInterpolator<ImageDataType> interpolator(array);
                    T valCur = T(0.0);
                    interpolator.Evaluate(py, px, &valCur);
                    if (valCur >= T(0.0))
                    {
                        // compute residual
                        T valRef = T(ptrGrayRef[m_y*w + m_x]);
                        residuals[0] = valRef - valCur;
                        return true;
                    }
                }
#endif
            }
        }
        residuals[0] = T(0.0);
        return true;
    }
    
private:
    cv::Mat m_depthRef;
    cv::Mat m_depthCurrent;
    cv::Mat m_grayRef;
    cv::Mat m_grayCur;
    Eigen::Matrix3d m_K;
    int m_x;
    int m_y;
    ceres::CostFunctionToFunctor<1, 2> m_intensityError;
};


class IntensityErrorNumeric
{
public:
    IntensityErrorNumeric(cv::Mat imgRef, cv::Mat imgCur, int x, int y) :
        m_imgRef(imgRef),
        m_imgCur(imgCur),
        m_x(x),
        m_y(y)
    {
    }
    
    virtual ~IntensityErrorNumeric() {}

    bool operator()(const double* const pt, double* residuals) const
    {
        const float* ptrImgRef = (const float*)m_imgRef.data;
        const float* ptrImgCur = (const float*)m_imgCur.data;
        int w = m_imgRef.cols;
        int h = m_imgRef.rows;

        double px = pt[0];
        double py = pt[1];
        if (px >= 0 && py >= 0 && px < w && py < h)
        {
            // lookup interpolated intensity
            double valCur = interpolate(ptrImgCur, px, py, w, h);
            if (!std::isnan(valCur))
            {
                // compute residual
                double valRef = static_cast<double>(ptrImgRef[m_y*w + m_x]);
                residuals[0] = valRef - valCur;
                return true;
            }
        }
        
        residuals[0] = 0.0;
        return true;
    }
    
protected:
    cv::Mat m_imgRef;
    cv::Mat m_imgCur;
    int m_x;
    int m_y;
};


class DvoErrorAuto
{
public:
    
    typedef ceres::NumericDiffCostFunction<IntensityErrorNumeric, ceres::CENTRAL, 1, 2>  IntensityErrorFunction;
    typedef IntensityErrorNumeric IntensityError;

    DvoErrorAuto(cv::Mat grayRef, cv::Mat depthRef,
                    cv::Mat grayCur, cv::Mat depthCur,
                    const Eigen::Matrix3d &K, int x, int y) :
        m_depthRef(depthRef),
        m_depthCurrent(depthCur),
        m_grayRef(grayRef),
        m_grayCur(grayCur),
        m_K(K),
        m_x(x),
        m_y(y),
        m_intensityError(new IntensityErrorFunction(new IntensityError(grayRef, grayCur, x, y)))
    {
    }
    
    virtual ~DvoErrorAuto()
    {
    }
    
    template <typename T>
    bool operator()(const T* const xi, T* residuals) const
    {
        T rot[3];
        rot[0] = xi[3];
        rot[1] = xi[4];
        rot[2] = xi[5];
        T t[3];
        t[0] = xi[0];
        t[1] = xi[1];
        t[2] = xi[2];
        
        const float* ptrDepthRef = (const float*)m_depthRef.data;
        int w = m_depthRef.cols;
        int h = m_depthRef.rows;
        T cx = T(m_K(0, 2));
        T cy = T(m_K(1, 2));
        T fx = T(m_K(0, 0));
        T fy = T(m_K(1, 1));
        T fxInv = T(1.0) / fx;
        T fyInv = T(1.0) / fy;

        // get depth
        size_t off = m_y*w + m_x;
        T dRef = T(ptrDepthRef[off]);
        if (dRef > T(0.0))
        {
            // project 2d point back into 3d using its depth
            T x0 = T((T(m_x) - cx) * fxInv);
            T y0 = T((T(m_y) - cy) * fyInv);
            
            // transform reference 3d point into current frame
            // rotation
            T pt3Ref[3];
            pt3Ref[0] = x0 * dRef;
            pt3Ref[1] = y0 * dRef;
            pt3Ref[2] = dRef;
            T pt3Cur[3];
            ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
            // translation
            pt3Cur[0] += t[0];
            pt3Cur[1] += t[1];
            pt3Cur[2] += t[2];

            if (pt3Cur[2] > T(0.0))
            {
                // project 3d point to 2d
                T pt2Cur[2];
                pt2Cur[0] = T((fx * pt3Cur[0] / pt3Cur[2]) + cx);
                pt2Cur[1] = T((fy * pt3Cur[1] / pt3Cur[2]) + cy);
#if 1
                // numeric differentiation
                return m_intensityError(pt2Cur, residuals);
#else
                // automatic differentiation
                T px = pt2Cur[0];
                T py = pt2Cur[1];
                if (px >= T(0) && py >= T(0) && px <= T(w-1) && py <= T(h-1))
                {
                    const float* ptrGrayRef = (const float*)m_grayRef.data;
                    const float* ptrGrayCur = (const float*)m_grayCur.data;
                    // lookup interpolated intensity
                    typedef ceres::Grid2D<float, 1> ImageDataType;
                    ImageDataType array(ptrGrayCur, 0, h, 0, w);
                    ceres::BiCubicInterpolator<ImageDataType> interpolator(array);
                    T valCur = T(0.0);
                    interpolator.Evaluate(py, px, &valCur);
                    if (valCur >= T(0.0))
                    {
                        // compute residual
                        T valRef = T(ptrGrayRef[m_y*w + m_x]);
                        residuals[0] = valRef - valCur;
                        return true;
                    }
                }
#endif
            }
        }
        residuals[0] = T(0.0);
        return true;
    }
    
private:
    cv::Mat m_depthRef;
    cv::Mat m_depthCurrent;
    cv::Mat m_grayRef;
    cv::Mat m_grayCur;
    Eigen::Matrix3d m_K;
    int m_x;
    int m_y;
    ceres::CostFunctionToFunctor<1, 2> m_intensityError;
};


// ### depth constancy / depth variation constraint

class DvoErrorDepthVariation
{

    public: 
        
        // constructor
        DvoErrorDepthVariation(     const int w,                // width
                                    const int h,                // height
                                    const float* grayRef,       // reference brightness
                                    const float* depthRef,      // reference depth
                                    const float* grayCur,       // current brightness
                                    const float* depthCur,      // current depth
                                    const double* intrinics,    // intrinsic parameters
                                    const int x,                // x-coordinate, or column
                                    const int y) :              // y-coordinate, or row
                    w_(w),
                    h_(h),
                    depthRef_(depthRef),
                    depthCurrent_(depthCur),
                    grayRef_(grayRef),
                    grayCur_(grayCur),
                    intrinics_(intrinics),
                    x_(x),
                    y_(y)
    {
    }
     
     
             // destructor
    virtual ~DvoErrorDepthVariation()
    {
    }
    
     
    template <typename T>
    bool operator()(const T* const pose, T* residuals) const
    {
        
                // rotation
                T rot[3];
		rot[0] = pose[3];
		rot[1] = pose[4];
		rot[2] = pose[5];
                
		// translation
		T t[3];
		t[0] = pose[0];
		t[1] = pose[1];
		t[2] = pose[2];
        
		// camera intrinsics
		T fx = T(intrinics_[0]);
		T fy = T(intrinics_[1]);
		T cx = T(intrinics_[2]);
		T cy = T(intrinics_[3]);
                
                // get depth for pixel
		T dRef = T(depthRef_[y_*w_ + x_]);
                T dCur = T(depthCurrent_[y_*w_ + x_]); 
                
                // check if the depth values are positive
                if ( ( dRef > T(0.0) ) && ( dCur > T(0.0) ) )
                {
                    
                    // project current 3d point directly 

                    // project 2d point back into 3d using its depth
                    T x0 = (T(x_) - cx) / fx;
                    T y0 = (T(y_) - cy) / fy;
                
                    // transform reference 3d point into current frame
                    T pt3Ref[3];
                    pt3Ref[0] = x0 * dRef;
                    pt3Ref[1] = y0 * dRef;
                    pt3Ref[2] = dRef;
                    
                    // rotate
                    T pt3Cur[3];
                    ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
                    
                    // translate
                    pt3Cur[0] += t[0];
                    pt3Cur[1] += t[1];
                    pt3Cur[2] += t[2];
                    
                    
                    if (pt3Cur[2] > T(0.0))
                    {
                                // project 3d point to 2d  
                                // pinhole camera projection 
                                T px = (fx * pt3Cur[0] / pt3Cur[2]) + cx;
                                T py = (fy * pt3Cur[1] / pt3Cur[2]) + cy;
                                
                        if (px >= T(0.0) && py >= T(0.0) && px <= T(w_-1) && py <= T(h_-1))
                        {
                            
                            typedef ceres::Grid2D<float, 1> ImageDataType;
                            
                            ImageDataType array(depthCurrent_, 0, h_, 0, w_);
                            
                            
                            // ### possibly we need to change the interpolation...
                            
                            ceres::BiCubicInterpolator<ImageDataType> interpolator(array);
                            
                            T valCur = T(0.0);
                            
                            interpolator.Evaluate(py, px, &valCur);
                            
                            //interpolate_depth<T>(py, px, valCur); 
                            
                            
                            
                            
                            if (valCur >= T(0.0))
                            {
                                // compute depth variation residual
                                T valRef = T(depthRef_[y_*w_ + x_]);
                                residuals[0] = valRef - valCur;
                                return true; 
                            }
                        }
                    }  
                }
        
                // invalid depth lookup
                residuals[0] = T(INVALID_RESIDUAL);
                return true;
        
    }
    
    
    private: 
        
        const int w_;
        const int h_;
        const float* depthRef_;
        const float* depthCurrent_;
        const float* grayRef_;
        const float* grayCur_;
        const double* intrinics_;
        const int x_;
        const int y_;
        
};


// ### Brightness constancy

class DvoErrorPhotometric
{
public:
	DvoErrorPhotometric(const int w, const int h,
						const float* grayRef, const float* depthRef,
						const float* grayCur, const float* depthCur,
						const double* intrinics, const int x, const int y) :
		w_(w),
		h_(h),
		depthRef_(depthRef),
        depthCurrent_(depthCur),
        grayRef_(grayRef),
        grayCur_(grayCur),
		intrinics_(intrinics),
        x_(x),
        y_(y)
    {
    }
    

    virtual ~DvoErrorPhotometric()
    {
    }
    

    template <typename T>
    bool operator()(const T* const pose, T* residuals) const
    {
		// rotation
                T rot[3];
		rot[0] = pose[3];
		rot[1] = pose[4];
		rot[2] = pose[5];
		// translation
		T t[3];
		t[0] = pose[0];
		t[1] = pose[1];
		t[2] = pose[2];
        
		// camera intrinsics
		T fx = T(intrinics_[0]);
		T fy = T(intrinics_[1]);
		T cx = T(intrinics_[2]);
		T cy = T(intrinics_[3]);

        // get depth for pixel
		T dRef = T(depthRef_[y_*w_ + x_]);
        if (dRef > T(0.0))
        {
            // project 2d point back into 3d using its depth
			T x0 = (T(x_) - cx) / fx;
			T y0 = (T(y_) - cy) / fy;
            
            // transform reference 3d point into current frame
            T pt3Ref[3];
            pt3Ref[0] = x0 * dRef;
            pt3Ref[1] = y0 * dRef;
            pt3Ref[2] = dRef;
			// rotate
			T pt3Cur[3];
            ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
            // translate
            pt3Cur[0] += t[0];
            pt3Cur[1] += t[1];
            pt3Cur[2] += t[2];

            if (pt3Cur[2] > T(0.0))
            {
                // project 3d point to 2d
				T px = (fx * pt3Cur[0] / pt3Cur[2]) + cx;
				T py = (fy * pt3Cur[1] / pt3Cur[2]) + cy;
                if (px >= T(0.0) && py >= T(0.0) && px <= T(w_-1) && py <= T(h_-1))
                {
                    // lookup interpolated intensity (automatic differentiation)
                    typedef ceres::Grid2D<float, 1> ImageDataType;
					ImageDataType array(grayCur_, 0, h_, 0, w_);
                    ceres::BiCubicInterpolator<ImageDataType> interpolator(array);
                    T valCur = T(0.0);
                    interpolator.Evaluate(py, px, &valCur);
                    if (valCur >= T(0.0))
                    {
                        // compute photometric residual
						T valRef = T(grayRef_[y_*w_ + x_]);
                        residuals[0] = valRef - valCur;
                        return true;
                    }
                }
            }
        }

		// invalid lookup
		residuals[0] = T(INVALID_RESIDUAL);
        return true;
    }
    
private:
	const int w_;
	const int h_;
	const float* depthRef_;
	const float* depthCurrent_;
    const float* grayRef_;
	const float* grayCur_;
	const double* intrinics_;
	const int x_;
	const int y_;
};


cv::Mat downsampleGray(const cv::Mat &gray)
{
	cv::Mat grayDown;
	cv::pyrDown(gray, grayDown, cv::Size(gray.cols / 2, gray.rows / 2));
    return grayDown;
}


cv::Mat downsampleDepth(const cv::Mat &depth)
{
	// downscaling by averaging the depth
	int w = depth.cols / 2;
	int h = depth.rows / 2;
    cv::Mat depthDown = cv::Mat::zeros(h, w, depth.type());
    for (int y = 0; y < h; ++y)
    {
		for (int x = 0; x < w; ++x)
        {
            int cnt = 0;
            float sum = 0.0f;
			float d0 = depth.at<float>(2 * y, 2 * x);
			if (d0 > 0.0f) { sum += d0; ++cnt; }
			float d1 = depth.at<float>(2 * y, 2 * x + 1);
			if (d1 > 0.0f) { sum += d1; ++cnt; }
			float d2 = depth.at<float>(2 * y + 1, 2 * x);
			if (d2 > 0.0f) { sum += d2; ++cnt; }
			float d3 = depth.at<float>(2 * y + 1, 2 * x + 1);
			if (d3 > 0.0f) { sum += d3; ++cnt; }
            if (cnt > 0)
				depthDown.at<float>(y, x) = sum / float(cnt);
        }
    }
    return depthDown;
}


void downsample(int numPyramidLevels, std::vector<Eigen::Matrix3d> &kPyramid,
	std::vector<cv::Mat> &grayRefPyramid, std::vector<cv::Mat> &depthRefPyramid, 
	 std::vector<cv::Mat> &grayCurPyramid, std::vector<cv::Mat> &depthCurPyramid)
{
	for (int i = 1; i < numPyramidLevels; ++i)
	{
		// downsample camera matrix
		Eigen::Matrix3d kDown = kPyramid[i - 1];
		kDown.topLeftCorner(2, 3) = kDown.topLeftCorner(2, 3) * 0.5;
		kPyramid.push_back(kDown);
		// downsample grayscale images
		grayRefPyramid.push_back(downsampleGray(grayRefPyramid[i - 1]));
		grayCurPyramid.push_back(downsampleGray(grayCurPyramid[i - 1]));
		// downsample depth images
		depthRefPyramid.push_back(downsampleDepth(depthRefPyramid[i - 1]));
		depthCurPyramid.push_back(downsampleDepth(depthCurPyramid[i - 1]));
	}
}


cv::Mat loadDepth(const std::string &filename)
{
	// read 16 bit depth image
	cv::Mat depth16 = cv::imread(filename, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
	// convert depth to float (1.0f = 1.0m)
	cv::Mat depth;
	depth16.convertTo(depth, CV_32FC1, (1.0 / 5000.0));
        
        /*
        // ### set zero values to 10 meters
        for (uint i = 0; i < depth16.rows; i++)
            for (uint j = 0; j < depth16.cols; j++){
                if (depth.at<float>(i, j) < 0.01f) 
                    depth.at<float>(i, j) = 10.0f; 
            }
        */
        
        
        return depth;
}


void loadData(cv::Mat &grayRef, cv::Mat &grayCur, cv::Mat &depthRef, cv::Mat &depthCur, Eigen::Matrix3d &K)
{
        
        std::string dataFolder = std::string(APP_SOURCE_DIR) + "/data/";
	cv::Mat grayRefIn = cv::imread(dataFolder + "rgb/1305031102.275326.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayRefIn.convertTo(grayRef, CV_32FC1, 1.0f / 255.0f);
	depthRef = loadDepth(dataFolder + "depth/1305031102.262886.png");
	cv::Mat grayCurIn = cv::imread(dataFolder + "rgb/1305031102.175304.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayCurIn.convertTo(grayCur, CV_32FC1, 1.0f / 255.0f);
	depthCur = loadDepth(dataFolder + "depth/1305031102.160407.png");
        
        /*
        std::string dataFolder = std::string(APP_SOURCE_DIR) + "/data/";
	cv::Mat grayRefIn = cv::imread(dataFolder + "rgbd_data7/rgb/48.802185.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayRefIn.convertTo(grayRef, CV_32FC1, 1.0f / 255.0f);
	depthRef = loadDepth(dataFolder + "rgbd_data7/depth/48.809997.png");
	cv::Mat grayCurIn = cv::imread(dataFolder + "rgbd_data7/rgb/48.834565.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayCurIn.convertTo(grayCur, CV_32FC1, 1.0f / 255.0f);
	depthCur = loadDepth(dataFolder + "rgbd_data7/depth/48.841358.png");
        */
        
        /*
        std::string dataFolder = std::string(APP_SOURCE_DIR) + "/data/";
	cv::Mat grayRefIn = cv::imread(dataFolder + "rgbd_data5/rgb/115.024210.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayRefIn.convertTo(grayRef, CV_32FC1, 1.0f / 255.0f);
	depthRef = loadDepth(dataFolder + "rgbd_data5/depth/115.024209.png");
	cv::Mat grayCurIn = cv::imread(dataFolder + "rgbd_data5/rgb/115.155453.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayCurIn.convertTo(grayCur, CV_32FC1, 1.0f / 255.0f);
	depthCur = loadDepth(dataFolder + "rgbd_data5/depth/115.155451.png");
        */
        
    // initialize intrinsic matrix
    K <<                517.3, 0.0, 318.6,
			0.0, 516.5, 255.3,
			0.0, 0.0, 1.0; 
}


double computeError(ceres::Problem &problem)
{
	// evaluate problem cost
	ceres::Problem::EvaluateOptions evalOptions;
	evalOptions.apply_loss_function = true;
	evalOptions.num_threads = 8;
	double errorEval;
	problem.Evaluate(evalOptions, &errorEval, 0, 0, 0);
	return errorEval;
}


int computeNumValidResiduals(const ceres::Problem &problem, const std::vector<double*> &parameters)
{
	std::vector<ceres::ResidualBlockId> problemResidualsBlocks;
	problem.GetResidualBlocks(&problemResidualsBlocks);
	int numValidResiduals = 0;
	for (size_t i = 0; i < problemResidualsBlocks.size(); ++i)
	{
		const ceres::CostFunction* costFunction = problem.GetCostFunctionForResidualBlock(problemResidualsBlocks[i]);
		double residual;
		bool eval = costFunction->Evaluate(&(parameters[0]), &residual, 0);
		if (eval && residual != INVALID_RESIDUAL)
			++numValidResiduals;
	}
	return numValidResiduals;
}






int main(int argc, char *argv[])
{
    
        // read dvo_ceres_settings
        string settings_file; 
        conf_dvo_ceres conf; 
        
        if (argc == 2) settings_file = argv[1];
        else settings_file = "settings.cfg";
        
        conf.load_dvo_ceres_settings(settings_file);
        
        if (conf.dump_dvo_ceres_settings_ == 1){
            cout << endl;
            conf.dump_dvo_ceres_settings();
        }
        
        // parameters
        int numPyramidLevels = conf._num_levels; 
        int maxLvl = numPyramidLevels - 1;
        int minLvl = 0;
        int maxIterations = conf._max_iterations; 
        int useHuber = conf._use_Huber; 
        int earlyBreak = conf._early_break; 
        double convergenceErrorRatio = conf._convergenceErrorRatio; 
        bool debug = false;
        
	// load RGB-D input data
	std::cout << "load input ..." << std::endl;
	cv::Mat grayRef, grayCur, depthRef, depthCur, orig_depthRef, orig_depthCur;
        Eigen::Matrix3d K;
        
        TumBenchmark* tumBenchmark_ = new TumBenchmark();  
        
        // ### 
        
        if (!tumBenchmark_->loadCamIntrinsics(conf._intrinsics_file, K)){
            std::cout << "Camera intrinsics file ('" << conf._intrinsics_file << "') could not be loaded! Using defaults..." << std::endl;
            // default intrinsics
            K <<    525.0f, 0.0f, 319.5f,
                    0.0f, 525.0f, 239.5f,
                    0.0f, 0.0f, 1.0f;
        }
        
        std::cout << K << std::endl; 
        
        /*
        K <<    517.3, 0.0, 318.6,
                0.0, 516.5, 255.3,
                0.0, 0.0, 1.0;
        */
        
        //loadData(grayRef, grayCur, depthRef, depthCur, K);

        
        std::vector<double> timestampsDepth;
        std::vector<double> timestampsColor;
        std::vector<std::string> filesDepth;
        std::vector<std::string> filesColor;
        
        if (!tumBenchmark_->loadAssoc(conf._data_assoc_file, timestampsDepth, filesDepth, timestampsColor, filesColor)){
            std::cout << "Assoc file ('" << conf._data_assoc_file << "') could not be loaded!" << std::endl;
            return false;
        }
        
        std::string path_depthRef = std::string(APP_SOURCE_DIR) + conf._data_folder + filesDepth[0]; 
        std::string path_depthCur = std::string(APP_SOURCE_DIR) + conf._data_folder + filesDepth[1]; 
        std::string path_colorRef = std::string(APP_SOURCE_DIR) + conf._data_folder + filesColor[0]; 
        std::string path_colorCur = std::string(APP_SOURCE_DIR) + conf._data_folder + filesColor[1]; 
        std::cout << path_depthRef << endl; 
        
        
        // ### now: take the first two images respectively, later: all required images
        
        cout << "loading files ...";
        cout.flush();     
        depthRef    = tumBenchmark_->loadDepth(path_depthRef);  
        depthCur    = tumBenchmark_->loadDepth(path_depthCur);  
        grayRef     = tumBenchmark_->loadColor_CV_32FC1(path_colorRef); 
        grayCur     = tumBenchmark_->loadColor_CV_32FC1(path_colorCur); 
        cout << " [done]" << endl; 
        
        
        
        /*
        imshow("grayCur", grayCur); 
        imshow("grayRef", grayRef); 
        cv::waitKey(0); 
        */
        
        // char c; cin >> c; 
        
        // ###
        
        
        // pyramid downsampling
	std::cout << "downsample input ..." << std::endl;
	std::vector<cv::Mat> grayRefPyr, depthRefPyr, grayCurPyr, depthCurPyr;
	std::vector<Eigen::Matrix3d> kPyr;
	grayRefPyr.push_back(grayRef);
	depthRefPyr.push_back(depthRef);
	grayCurPyr.push_back(grayCur);
	depthCurPyr.push_back(depthCur);
	kPyr.push_back(K);
	downsample(numPyramidLevels, kPyr, grayRefPyr, depthRefPyr, grayCurPyr, depthCurPyr);

	// initialize pose (angle-axis and translation)
	Eigen::Matrix<double, 6, 1> pose;
	pose.setZero();
	if (debug)
	{
		// translation
		pose.topRows<3>() = Eigen::Vector3d::Zero();
		// rotation
		Eigen::Matrix3d rot = Eigen::Matrix3d::Identity();
		Eigen::AngleAxisd aa(rot);
		pose.bottomRows<3>() = aa.angle() * aa.axis();
		std::cout << "pose initial = " << pose.bottomRows<3>().transpose() << std::endl;
	}
        
        
	// estimate pose using direct dense image alignment
	std::cout << "coarse-to-fine optimization ..." << std::endl;
        
        orig_depthRef = depthRef.clone();
        orig_depthCur = depthCur.clone();

        if (conf._depth_filter_type == AVG_FILTER){
            std::cout << "running wavg filter" << std::endl; 
            weighted_average_filter(depthRef); 
            weighted_average_filter(depthCur); 
        }
    
        if (conf._depth_filter_type ==  BILATERAL_FILTER){
            std::cout << "running bilateralFilter" << std::endl; 
            bilateralFilter(orig_depthRef, depthRef, 3.0); 
            bilateralFilter(orig_depthCur, depthCur, 3.0); 
        }
     
        if (conf._depth_filter_type == MEDIAN_FILTER){
            std::cout << "running median filter" << std::endl; 
            median_filter(depthRef); 
            median_filter(depthCur); 
        } 
        
        
        double tmr = (double)cv::getTickCount();
	for (int lvl = maxLvl; lvl >= minLvl; --lvl)
	{
		std::cout << "   level " << lvl << "..." << std::endl;
		double tmrLvl = (double)cv::getTickCount();
		grayRef = grayRefPyr[lvl];
		depthRef = depthRefPyr[lvl];
		grayCur = grayCurPyr[lvl];
		depthCur = depthCurPyr[lvl];
		double intrinsics[4] { kPyr[lvl](0, 0), kPyr[lvl](1, 1), kPyr[lvl](0, 2), kPyr[lvl](1, 2) };

		// instantiate problem
		ceres::Problem problem;
                
                // ### loss function for the brightness constancy
		ceres::LossFunction* lossFunction = 0;
		if (useHuber)
			lossFunction = new ceres::HuberLoss(50.0f / 255.0f);              // 50 grayscale steps
                

                ceres::LossFunction* lossFunctionBrightnessScaled = 0;
                lossFunctionBrightnessScaled = new ceres::ScaledLoss(lossFunction, conf._alpha, ceres::TAKE_OWNERSHIP); 

                
                
                // ### a scaled loss function for the depth variance
                
                ceres::LossFunction* lossFunctionDepth = 0;
		if (useHuber){
			lossFunctionDepth = new ceres::HuberLoss(10.0f / 100.0f);          // 10 cm       
                }
                
                ceres::LossFunction* lossFunctionDepthScaled = 0;
                lossFunctionDepthScaled = new ceres::ScaledLoss(lossFunctionDepth, conf._beta, ceres::TAKE_OWNERSHIP); 
                
                
		const int w = grayRef.cols;
		const int h = grayRef.rows;
                
                // ### 2x because there are 2 terms now
		//const size_t numResiduals = 2 * (size_t)w * (size_t)h;
                //const size_t numResiduals = (size_t)w * (size_t)h;
                
                // ### -> "reference" is transformed to the "current" frame
                // ### -> add only residuals for non-zero depth values
                
                const size_t numResiduals = (size_t)w * (size_t)h + (size_t) (w * h - n_zero_values(depthRef)); 
                

		std::vector<ceres::ResidualBlockId> residualsBlocks(numResiduals, 0);
		std::vector<double*> parameters;
		parameters.push_back(pose.data());

		double errorAvgLast = std::numeric_limits<double>::max();
                
                // ### with maxIterations, # of iterations is controlled externally, i.e.,
                // ### ceres solver launches for one single iteration
		for (int itr = 0; itr < maxIterations; ++itr)
		{
			std::cout << "   iteration " << itr << ":" << std::endl;

                        uint depth_index_counter = 0; 
                        
			// update residual blocks
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
                                    
					const size_t id = (size_t)(y*w + x);
                                        
                                        // ### add brightness constancy residual

					DvoErrorPhotometric* costPhotometric = new DvoErrorPhotometric(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y); 
                                        
					ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorPhotometric, 1, 6>(costPhotometric);
                                        
                                           
                                        /*
                                        DvoErrorAuto* costPhotometric = new DvoErrorAuto(
						grayRef, depthRef, 
						grayCur, depthCur, 
						kPyr[lvl], x, y); 
                                        
                                        */
                                        
                                        //ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorAuto, 1, 6>(costPhotometric); 
                                        
					double residual;
                                        
					bool eval = costFunction->Evaluate(&(parameters[0]), &residual, 0);
                                        
					if (eval && residual != INVALID_RESIDUAL)
					{
						if (!residualsBlocks[id])
						{
							residualsBlocks[id] = problem.AddResidualBlock(costFunction, lossFunctionBrightnessScaled, pose.data());
						}
					}
					else
					{
						if (residualsBlocks[id])
						{
							problem.RemoveResidualBlock(residualsBlocks[id]);
							residualsBlocks[id] = 0;
						}
						delete costFunction;
					}
					
					
					// ### add depth variation residual
					
					///*
					
					
					//const size_t id_depth = (size_t)(w*h + y*w + x);
					const size_t id_depth = (size_t)(w*h + depth_index_counter); 
					
					if ( orig_depthRef.at<float>(y, x) > 0.01f )
                                        {
			
#if 0               // the common depth variation term with bicubic interpolation                         
                                            
					DvoErrorDepthVariation* costDepthVariation = new DvoErrorDepthVariation(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y);
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthVariation, 1, 6>(costDepthVariation);
                                            
#else               // interpolation with accounting for the weights
                                        
                                        DvoErrorAutoDepth* costDepthVariation = new DvoErrorAutoDepth(
						grayRef, depthRef, 
						grayCur, depthCur, 
						kPyr[lvl], x, y); 
                                        
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorAutoDepth, 1, 6>(costDepthVariation);
#endif                                        
                                        
                                        
                                        
                                        double residualDepth; 
                                        
                                        bool evalDepth = costFunctionDepth->Evaluate(&(parameters[0]), &residualDepth, 0);
					
					if (evalDepth && residualDepth != INVALID_RESIDUAL)
					{
						if (!residualsBlocks[id_depth])
						{
							residualsBlocks[id_depth] = problem.AddResidualBlock(costFunctionDepth, lossFunctionDepthScaled, pose.data());
						}
					}
					else
					{
						if (residualsBlocks[id_depth])
						{
							problem.RemoveResidualBlock(residualsBlocks[id_depth]);
							residualsBlocks[id_depth] = 0;
						}
						delete costFunctionDepth;
					}
					
					
					depth_index_counter++;
					
					
                                        }
					//*/
					
				}
				
			}
			
			int numValidResidualsBefore = problem.NumResidualBlocks();
			if (numValidResidualsBefore == 0)
				continue;

			// solve NLS optimization problem
			ceres::Solver::Options options;
                        // a single iteration, # of iterations is controlled externally in the loop
			options.max_num_iterations = 1;                              
			options.num_threads = 8;
			options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
			options.minimizer_progress_to_stdout = false;
			options.logging_type = ceres::SILENT;

			ceres::Solver::Summary summary;
			ceres::Solve(options, &problem, &summary);
			if (debug)
				std::cout << summary.FullReport() << std::endl;

			int numValidResidualsAfter = computeNumValidResiduals(problem, parameters);

			double error = summary.final_cost;
			double errorAvg = error / numValidResidualsAfter;
#if 0
			std::cout << "      error " << error << std::endl;
			std::cout << "      avg error last " << errorAvgLast << std::endl;
			std::cout << "      avg error " << errorAvg << std::endl;
			std::cout << "      num valid residuals before " << numValidResidualsBefore << std::endl;
			std::cout << "      num valid residuals after " << numValidResidualsAfter << std::endl;
#endif

#if 0
			double errorEval = computeError(problem);
			//std::cout << "      error eval: " << errorEval << std::endl;
#endif

			// break if no improvement
			if (earlyBreak && errorAvg / errorAvgLast > convergenceErrorRatio)
			{
				errorAvgLast = errorAvg;
				break;
			}

			errorAvgLast = errorAvg;
		}
		
		tmrLvl = ((double)cv::getTickCount() - tmrLvl) / cv::getTickFrequency();
		std::cout << "   runtime for level " << lvl << ": " << tmrLvl << std::endl;
                
    } // direct dense image alignment done
    
    
    // post-processing
    
    tmr = ((double)cv::getTickCount() - tmr)/cv::getTickFrequency();
    std::cout << "runtime: " << tmr << std::endl;

	// final result
	std::cout << "pose final = " << pose.transpose() << std::endl;
	if (debug)
	{
		// compare with expected result (approx without Huber norm)
		Eigen::Matrix<double, 6, 1> poseExpected;
		// SE3 result
		//poseExpected << -0.0021, 0.0057, 0.0374, -0.0292, -0.0183, -0.0009;
		// angle-axis result
		//poseExpected << -0.00243882, 0.00624604, 0.0372902, -0.0292, -0.0183, -0.0009;
		//std::cout << "pose expected = " << poseExpected.transpose() << std::endl;
	}
    
    
    
        // ### translation
        Eigen::Vector3d translation = pose.topRows<3>(); 
        std::cout << "TRANSLATION: " << translation << std::endl; 
                
        Eigen::Vector3d v = pose.bottomRows<3>();
        double angle = v.norm(); //sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
        
        Eigen::Vector3d axis = v.normalized(); 
        //axis[0] = v[0] / angle;
        //axis[1] = v[1] / angle;
        //axis[2] = v[2] / angle;
        
        Eigen::AngleAxisd bb(angle, axis); 
        Eigen::Matrix3d POSE = bb.toRotationMatrix(); 
        
        
        RgbdProcessing* rgbdProc_ = new RgbdProcessing();
        
        /*
        Eigen::Matrix3f K_;
        
         K_ <<    517.3, 0.0, 318.6,
			0.0, 516.5, 255.3,
			0.0, 0.0, 1.0;
        */
        
        cv::Mat vertexMapRef; 
        rgbdProc_->computeVertexMap(K, depthRef, vertexMapRef); 
        //rgbdProc_->computeVertexMap(K_, orig_depthRef, vertexMapRef); 
        
        cv::Mat vertexMapCur; 
        rgbdProc_->computeVertexMap(K, depthCur, vertexMapCur); 
        //rgbdProc_->computeVertexMap(K_, orig_depthCur, vertexMapCur); 
        
        //uint NUM_POINTS = mask_num_points(segment_masks[s][minLvl]); 
        Eigen::MatrixXd pc_ref, pc_cur, pc_cur_rotated;
               
        
        // ### create white segment_masks
        
        cv::Mat segment1(depthRef.rows, depthRef.cols, CV_8UC1, cv::Scalar(255)); 
        
        extract_point_cloud(pc_ref, segment1, vertexMapRef); 
        extract_point_cloud(pc_cur, segment1, vertexMapCur); 

        //pc_cur_rotated = pc_ref * POSE.inverse();
        //pc_cur_rotated = pc_cur_rotated.rowwise() + translation.transpose(); 
        
        pc_cur_rotated = pc_cur * POSE;
        pc_cur_rotated = pc_cur_rotated.rowwise() - translation.transpose(); 
         
        
        string ref_ply = "./ref.ply"; 
        string cur_ply = "./cur.ply"; 
        string cur_rot_ply = "./cur_rot.ply"; 
        
        write_ply_file(ref_ply, pc_ref, nullptr, {0, 255, 255}, 2);
        write_ply_file(cur_ply, pc_cur, nullptr, {255, 0, 255}, 2);
        write_ply_file(cur_rot_ply, pc_cur_rotated, nullptr, {255, 255, 0}, 2);
        
        std::cout << "written" << std::endl; 
        
        char f; std::cin >> f;
        
        

    //}
    
    // ### 
    // ### loop finished ( number of segments )
    // ### 
    
    // clean up
    cv::destroyAllWindows();
    std::cout << "direct image alignment finished." << std::endl;
    
    return 0;
}
