

double interpolate(const float* ptrImgIntensity, double x, double y, int w, int h)
{
    double valCur = std::numeric_limits<double>::quiet_NaN();
    
#if 0
    // direct lookup, no interpolation
    int x0 = static_cast<int>(std::floor(x + 0.5));
    int y0 = static_cast<int>(std::floor(y + 0.5));
    if (x0 >= 0 && x0 < w && y0 >= 0 && y0 < h)
        valCur = ptrImgIntensity[y0*w + x0];
#else
    //bilinear interpolation
    int x0 = static_cast<int>(std::floor(x));
    int y0 = static_cast<int>(std::floor(y));
    int x1 = x0 + 1;
    int y1 = y0 + 1;
    
    double x1_weight = x - static_cast<double>(x0);
    double y1_weight = y - static_cast<double>(y0);
    double x0_weight = 1.0 - x1_weight;
    double y0_weight = 1.0 - y1_weight;
    
    if (x0 < 0 || x0 >= w)
        x0_weight = 0.0;
    if (x1 < 0 || x1 >= w)
        x1_weight = 0.0;
    if (y0 < 0 || y0 >= h)
        y0_weight = 0.0;
    if (y1 < 0 || y1 >= h)
        y1_weight = 0.0;
    double w00 = x0_weight * y0_weight;
    double w10 = x1_weight * y0_weight;
    double w01 = x0_weight * y1_weight;
    double w11 = x1_weight * y1_weight;
    
    double sumWeights = w00 + w10 + w01 + w11;
    double sum = 0.0;
    if (w00 > 0.0)
        sum += static_cast<double>(ptrImgIntensity[y0*w + x0]) * w00;
    if (w01 > 0.0)
        sum += static_cast<double>(ptrImgIntensity[y1*w + x0]) * w01;
    if (w10 > 0.0)
        sum += static_cast<double>(ptrImgIntensity[y0*w + x1]) * w10;
    if (w11 > 0.0)
        sum += static_cast<double>(ptrImgIntensity[y1*w + x1]) * w11;
    
    if (sumWeights > 0.0)
        valCur = sum / sumWeights;
#endif
    
    return valCur;
}


class IntensityErrorNumeric
{
public:
    IntensityErrorNumeric(cv::Mat imgRef, cv::Mat imgCur, int x, int y) :
        m_imgRef(imgRef),
        m_imgCur(imgCur),
        m_x(x),
        m_y(y)
    {
    }
    
    virtual ~IntensityErrorNumeric() {}

    bool operator()(const double* const pt, double* residuals) const
    {
        const float* ptrImgRef = (const float*)m_imgRef.data;
        const float* ptrImgCur = (const float*)m_imgCur.data;
        int w = m_imgRef.cols;
        int h = m_imgRef.rows;

        double px = pt[0];
        double py = pt[1];
        if (px >= 0 && py >= 0 && px < w && py < h)
        {
            // lookup interpolated intensity
            double valCur = interpolate(ptrImgCur, px, py, w, h);
            if (!std::isnan(valCur))
            {
                // compute residual
                double valRef = static_cast<double>(ptrImgRef[m_y*w + m_x]);
                residuals[0] = valRef - valCur;
                return true;
            }
        }
        
        residuals[0] = 0.0;
        return true;
    }
    
protected:
    cv::Mat m_imgRef;
    cv::Mat m_imgCur;
    int m_x;
    int m_y;
};


class DvoErrorAuto
{
public:
    
    typedef ceres::NumericDiffCostFunction<IntensityErrorNumeric, ceres::CENTRAL, 1, 2>  IntensityErrorFunction;
    typedef IntensityErrorNumeric IntensityError;

    DvoErrorAuto(cv::Mat grayRef, cv::Mat depthRef,
                    cv::Mat grayCur, cv::Mat depthCur,
                    const Eigen::Matrix3d &K, int x, int y) :
        m_depthRef(depthRef),
        m_depthCurrent(depthCur),
        m_grayRef(grayRef),
        m_grayCur(grayCur),
        m_K(K),
        m_x(x),
        m_y(y),
        m_intensityError(new IntensityErrorFunction(new IntensityError(grayRef, grayCur, x, y)))
    {
    }
    
    virtual ~DvoErrorAuto()
    {
    }
    
    template <typename T>
    bool operator()(const T* const xi, T* residuals) const
    {
        T rot[3];
        rot[0] = xi[3];
        rot[1] = xi[4];
        rot[2] = xi[5];
        T t[3];
        t[0] = xi[0];
        t[1] = xi[1];
        t[2] = xi[2];
        
        const float* ptrDepthRef = (const float*)m_depthRef.data;
        int w = m_depthRef.cols;
        int h = m_depthRef.rows;
        T cx = T(m_K(0, 2));
        T cy = T(m_K(1, 2));
        T fx = T(m_K(0, 0));
        T fy = T(m_K(1, 1));
        T fxInv = T(1.0) / fx;
        T fyInv = T(1.0) / fy;

        // get depth
        size_t off = m_y*w + m_x;
        T dRef = T(ptrDepthRef[off]);
        if (dRef > T(0.0))
        {
            // project 2d point back into 3d using its depth
            T x0 = T((T(m_x) - cx) * fxInv);
            T y0 = T((T(m_y) - cy) * fyInv);
            
            // transform reference 3d point into current frame
            // rotation
            T pt3Ref[3];
            pt3Ref[0] = x0 * dRef;
            pt3Ref[1] = y0 * dRef;
            pt3Ref[2] = dRef;
            T pt3Cur[3];
            ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
            // translation
            pt3Cur[0] += t[0];
            pt3Cur[1] += t[1];
            pt3Cur[2] += t[2];

            if (pt3Cur[2] > T(0.0))
            {
                // project 3d point to 2d
                T pt2Cur[2];
                pt2Cur[0] = T((fx * pt3Cur[0] / pt3Cur[2]) + cx);
                pt2Cur[1] = T((fy * pt3Cur[1] / pt3Cur[2]) + cy);
#if 0
                // numeric differentiation
                return m_intensityError(pt2Cur, residuals);
#else
                // automatic differentiation
                T px = pt2Cur[0];
                T py = pt2Cur[1];
                if (px >= T(0) && py >= T(0) && px <= T(w-1) && py <= T(h-1))
                {
                    const float* ptrGrayRef = (const float*)m_grayRef.data;
                    const float* ptrGrayCur = (const float*)m_grayCur.data;
                    // lookup interpolated intensity
                    typedef ceres::Grid2D<float, 1> ImageDataType;
                    ImageDataType array(ptrGrayCur, 0, h, 0, w);
                    ceres::BiCubicInterpolator<ImageDataType> interpolator(array);
                    T valCur = T(0.0);
                    interpolator.Evaluate(py, px, &valCur);
                    if (valCur >= T(0.0))
                    {
                        // compute residual
                        T valRef = T(ptrGrayRef[m_y*w + m_x]);
                        residuals[0] = valRef - valCur;
                        return true;
                    }
                }
#endif
            }
        }
        residuals[0] = T(0.0);
        return true;
    }
    
private:
    cv::Mat m_depthRef;
    cv::Mat m_depthCurrent;
    cv::Mat m_grayRef;
    cv::Mat m_grayCur;
    Eigen::Matrix3d m_K;
    int m_x;
    int m_y;
    ceres::CostFunctionToFunctor<1, 2> m_intensityError;
};
